# simple_droplet_tools

A set of simple tools for microfluidic droplet analysis in ImageJ  

### droplet adjust tool
The droplet adjust tool is used to generate .jpg images for visualization purposes, adjusting contrast and saturation of the image and adding a scale bar.

### droplet quantify tool
The droplet quantifier tool applies a simple thresholding approach on the brightfield image of the droplets and then generates ROIs, which are used as input of the particle analyzer plug-in to measure raw fluorescence intensities.
The macro produces 3 output files:
- .zip list of ROIs
- .jpg image of the ROI masks and ID numbers
- .xlsx table containing the results of the particle analyzer  

**options**  
Right clicking on the droplet quantify tool, will prompt a series of dialogs to set the options:
- an integer number for the channel to quantify
-	a string describing the channel type (brightfield or fluorescence)
-	as string describing the color of each channels (for visualization purposes)  

The entire set of options can be accessed only from the source code.

### installation
Before installing the macro, take the time to read the source the code, it is fully commented and it describes all the steps.  
You can open and customize the source code to your needs. Commented lines will lead you through the configuration. Please, make sure that the configuration matches your input image, otherwise it might result in unwanted behavior.  
The features to be measured can be defined by selecting them in `Analyze > Set Measurement...` for more information check the relevant [ImageJ documentation](https://imagej.nih.gov/ij/docs/menus/analyze.html#set).  

To install the macro open the `Macro > Install...` dialog and select the .ijm file.
