/* This macro was created by Marco R Cosenza
 * feel free to modify/reuse/share except this heading,
 * please remember to acknowledge its usage
 * for any information, please contact: marco.r.cosenza@gmail.com
 */

///////////////////////////////////////////////////////////////////////////
// PLEASE SET THE VARIABLES BELOW MANUALLY OR SET THE BASIC ONES THROUGH //
// THE OPTIONS BY DOUBLE-CLICKING THE CORRESPONDING TOOL ICON            //
///////////////////////////////////////////////////////////////////////////

//define here the optimal minimum and maximum pixel intensity values
//for the brightfield channel
var	min = 1000;
var	max = 7600;

//define channel saturation for ALL fluorescence channels, from 0 to 1
//0.35 is a good starting point
var saturation = 0.35;

//List here the channel type in the same order that is in the image
//"b" is for a brightfield channel, "f" for a fluorescence channel
//IMPORTANT:
//do not forget the quote signs, use only one space to separate letters
var channels = "b f";
//Same for the color types for the fluorescence channels
//do not forget to write the color name with a capital letter
var colors = "Green";

//if you have an image with exactly 3 channels, but you want to save an image with
//only the first two, set switch1 to 1, otherwise set it to 0
var switch1 = 0;

//define which channels you want to save in the final image, following the original
//order use 0 for not-save and 1 for save, for example "011" would save only
//channel 2 and 3, but not channel 1.
//if you want to save all, use -1 or a string of ones, one for each channel, e.g.: "111"
var channels_to_save = "11";

//if you want to add a scale bar, set to true
var scale_bar = true;

//Where to measure pixel intensity
var measure_channel = 2;

///////////////////////////////////////////////////////////////////////////
// OK. YOU'RE DONE. If there is some mistake in the descriptions above,  //
// you should get an error message with an hint when you apply the macro //
///////////////////////////////////////////////////////////////////////////


var charr = split(channels, " /");
var colarr = split(colors, " /");
var	ask = 0;



macro "Droplet adjust Tool - C00cO11cc" {
	id = getImageID();
//reads the location and name of the file
	path = getInfo("image.directory")+getInfo("image.filename");
//some initial checks for the channel and color descriptions
	Stack.getDimensions(w,h,ch,sl,fr);
	fcount = 0;
	for(chn = 0; chn < charr.length; chn++){
		if(charr[chn] == "f"){
			fcount++;
		}
	}
	if(colarr.length != fcount){
		exit("The color description does not match the number of fluorescence channels");
	}
	if(charr.length != ch){
		exit("The channel type description does not match the actual channel number of the image");
	}
	if(lengthOf(channels_to_save) != ch){
		exit("The channels to save description does not match the actual channel number of the image");
	}


//check if more images are open and asks whether to process all
	n = nImages;
	if(n>1){
		ask = getBoolean("Do you want to modify all images?");
		if(ask==0) { n = 1;
		}
	}

//select the wanted image or go to process all depending on user answer
	if (ask == true) {
		toProcess = newArray(nImages); 
			for(t=0; t < nImages; t++){
				selectImage(t+1);
				toProcess[t] = getTitle(); 
			}
	} else  {
		toProcess = newArray(1);
		toProcess[0] = getTitle();
	}
	

	Array.print(toProcess);
	//for(i=0; i < n; i++){
	//	if(ask==0){ selectImage(id);
	//	} else {
	//	selectImage(i+1);
	//	}
	for (k = 0; k < toProcess.length; k++) {
		inImg = toProcess[k];
		selectImage(inImg);
		path = getInfo("image.directory")+getInfo("image.filename");
//makes a composite image with just brightfield and green
		run("Make Composite");

//converts the image in jpg compatible format
		//run("RGB Color");
		f_count=0;
		for(j=0; j<ch; j++){
			if(ch > 1){
				Stack.setChannel(j+1);
			}
			if(charr[j] == "b"){
//reset brightness&contrast of droplet channel
				run("Grays");
				resetMinAndMax();
			} else if(charr[j] == "f"){
//automatic setting of brightness&contrast of fluorescence channel
				color = colarr[f_count];
				run(color);
				run("Enhance Contrast", "saturated="+saturation);
				f_count++;
			} else {
				exit("There is something wrong in the channel type description");
			}
		}

//activate only channels that were chosen to be saved
		if(channels_to_save == -1){
			channel_string = "";
			for (l = 0; l < ch; l++) {
				channel_string = channel_string+1;
			}
			Stack.setActiveChannels(channel_string);
		} else {
			Stack.setActiveChannels(channels_to_save);
		}

//add scale bar
		if(scale_bar) {
			run("Scale Bar...", "width=100 height=12 font=42 color=White background=None location=[Lower Right] bold overlay label");
		}

//saves and closes the image
		saveAs("Jpeg", path+" out.jpg");
		//close(inImg);
	}
}

macro "Droplet adjust Tool Options" {

	min = getNumber("min :", min);
	max = getNumber("max :", max);
	saturation = getNumber("saturation :", saturation);
}



macro "Droplet Qtfy Tool - C00cV11cc" {

path = getInfo("image.directory")+getInfo("image.filename");
img = getTitle();

channels_arr = split(channels, " ");
brightfield_ch = 0;
for (i = 0; i < channels_arr.length; i++) {
	if (channels_arr[i] == "b") {
		brightfield_ch = i+1;
	}
}
if (brightfield_ch == 0) {
	exit("brightfield channel could not be found");
}
IJ.log("brightfiel channel: "+brightfield_ch);

//set options
run("Options...", "iterations=1 count=1 black");
run("Set Measurements...", "area mean min centroid center perimeter shape integrated stack redirect=None decimal=3");

//creat droplet mask
run("Duplicate...", "title=[droplet selection] duplicate channels="+brightfield_ch);
run("Enhance Contrast...", "saturated=10");
run("Robust Automatic Threshold Selection", "noise=25 lambda=3 min=322");
//run("Invert");

//analyze particles and save droplet mask image
//clean-up rois and results
run("Clear Results");
n = roiManager("Count");
if(n>0){
	roiManager("Delete");
}


run("Analyze Particles...", "size=200-Infinity pixel circularity=0.60-1.00 show=Outlines display exclude add in_situ");

saveAs("Jpeg", path+" droplet selection.jpg");
roiManager("Save", path+" droplet roi.zip");
close();

selectImage("droplet selection");
close();

//measure
//run("Duplicate...", "title=[fluo channel] duplicate");
selectImage(img);
Stack.setChannel(measure_channel);
if (measure_channel == -1) {
	roiManager("multi-measure measure_all");
} else {
	Stack.setChannel(measure_channel);
	roiManager("Measure");	
}
roiManager("deselect");
saveAs("Results", path+" droplet data.xls");

}

macro "Droplet Qtfy Tool Options" {
	ch_measure = "Insert here the number corresponding to the channel to quantify, set to -1 to measure all channels"
	ch_string = "List here the channel type in the same order that is in the image " +
    			"'b' is for a brightfield channel, 'f' for a fluorescence channel"
	ch_colors = "List here the color of the fluorescent channels in the same order that is in the image " +
				"start the color name with a capital letter"
	measure_channel = getNumber(ch_measure, 2);
	channels = getString(ch_string, "b f");
	colors = getString(ch_colors, "Green Red");
}
